// https://gitlab.com/Amaltas/leetcode/-/raw/main/files/Screenshot%202023-10-14%20at%2010.14.31%20PM.png?ref_type=heads
//
// Compile and run:
// $ clang++ -std=c++20 -o rotary-lock rotary-lock.cc
// $ ./rotary-lock

#include <array>
#include <cassert>
#include <vector>

constexpr std::array<std::array<int, 10>, 10> kDistanceTable{{
    /*       1, 2, 3, 4, 5, 6, 7, 8, 9, 10 */
    /* 1 */ {0, 1, 2, 3, 4, 5, 4, 3, 2, 1},
    /* 2 */ {1, 0, 1, 2, 3, 4, 5, 4, 3, 2},
    /* 3 */ {2, 1, 0, 1, 2, 3, 4, 5, 4, 3},
    /* 4 */ {3, 2, 1, 0, 1, 2, 3, 4, 5, 4},
    /* 5 */ {4, 3, 2, 1, 0, 1, 2, 3, 4, 5},
    /* 6 */ {5, 4, 3, 2, 1, 0, 1, 2, 3, 4},
    /* 7 */ {4, 5, 4, 3, 2, 1, 0, 1, 2, 3},
    /* 8 */ {3, 4, 5, 4, 3, 2, 1, 0, 1, 2},
    /* 9 */ {2, 3, 4, 5, 4, 3, 2, 1, 0, 1},
    /* 10 */ {1, 2, 3, 4, 5, 4, 3, 2, 1, 0},
}};

uint64_t getMinCodeEntryTime(int N, int M, std::vector<int> C) {
  // Write your code here
  int current_position = 1;
  int seconds = 0;
  for (int i : C) {
    seconds += kDistanceTable[current_position - 1][i - 1];
    current_position = i;
  }
  return seconds;
}

int main(int argc, char** argv) {
  assert(getMinCodeEntryTime(3, 3, {1, 2, 3}) == 2);
  assert(getMinCodeEntryTime(10, 4, {9, 4, 4, 8}) == 11);
}