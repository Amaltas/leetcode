// https://gitlab.com/Amaltas/leetcode/-/raw/main/files/Screenshot_2023-10-14_at_4.06.02_AM.png
//
// Compile and run.
// $ clang++ -std=c++20 -o photography photography.cc
// $ ./photography

#include <cassert>
#include <string>

// Write any include statements here

int GetArtisticPhotographCount(int N, std::string C, int X, int Y) {
  // Write your code here
  int counter = 0;
   std::size_t i = 0;
   char middle_char = 'A';
   char last_char;
   while (i < C.size()) {
     char c = C.at(i);
     if (!(c == 'P' || c == 'B')) { 
       i++;
       continue;
     } 
 
     if (c == 'P') { 
       last_char = 'B';
     } else { 
       last_char = 'P';
     } 
 
     for (int a = X; a <= Y; ++a) {
       if (i + a >= N) break;
       if (C.at(i + a) == middle_char) {
         for (int b = X; b <= Y; ++b) {
           if (i + a + b >= N) break;
           if (C.at(i + a + b) == last_char) {
             counter++;
           }
         }
       }
     } 
 
     i++;
   }
   return counter;
}

int main(int argc, char** argv) {
  assert(GetArtisticPhotographCount(5, "APABA", 1, 2) == 1);
  assert(GetArtisticPhotographCount(5, "APABA", 2, 3) == 0);
  assert(GetArtisticPhotographCount(8, ".PBAAP.B", 1, 3) == 3);
}
