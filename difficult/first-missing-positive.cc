// https://leetcode.com/problems/first-missing-positive/description/
//
// The solution works in the constraints mentioned in the problem, that is n <= 10^5

#include <bitset>
#include <cassert>
#include <iostream>
#include <vector>

namespace leetcode {

constexpr bool IsLittleEndian() {
  int32_t a = 1;

  if ((a & 0x000000ff) == 1) {
    return true;
  }

  return false;
}

int firstMissingPositive(std::vector<int32_t> arr) {
  // Since we know 10^5 fits in 17 bits, we utilize first (or last) most significant byte as marker
  // for index in array that occupies positive integer. We convert negative numbers to
  // zero so first byte is always zero.
  constexpr int32_t mask = IsLittleEndian() ? 0xff000000 : 0x000000ff;
  int n = arr.size();

  for (int i = 0; i < n; i++) {
    if (auto a = (arr[i] & ~mask); a > 0 && a <= n) {
      if (arr[a - 1] < 0 || arr[a - 1] > n) {
        arr[a - 1] = mask;
      } else {
        arr[a - 1] |= mask;
      }
    }
  }

  for (int i = 0; i < n; i++) {
    if  ((arr[i] & mask) == 0) {
      return i + 1;
    }
  }

  return n +  1;
}

}  // namespace leetcode

using leetcode::firstMissingPositive;

// Driver code
int main() {
  assert(firstMissingPositive({0, 10, 2, -10, -20}) == 1);
  assert(firstMissingPositive({7,8,9,11,12}) == 1);
  assert(firstMissingPositive({1,2,0}) == 3);
  assert(firstMissingPositive({1,2,3}) == 4);
  assert(firstMissingPositive({3,4,-1,1}) == 2);
  return 0;
}
