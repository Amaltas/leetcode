// https://gitlab.com/Amaltas/leetcode/-/raw/main/files/Screenshot%202023-10-14%20at%209.15.02%20PM.png?ref_type=heads
//
// Compile and run:
// $ clang++ -std=c++20 -o kaitenzushi kaitenzushi.cc
// ./kaitenzushi

#include <cassert>
#include <cmath>
#include <vector>
 
 
int getMaximumEatenDishCount(int N, std::vector<int> D, int K) { 
   // Write your code here
 
   // We shard the K keys in different buckets to optimze memory use and choice
   // of container. Since D is 1,000,000 (the max in constraints), any solution
   // needs max 1,000,000 items in worst case.
   // Instead we store the cache keys in 20000 buckets, each bucket containing
   // metadata of 50 keys range. So for whatever K size the cache is fixed size
   // of 20000.
   constexpr int num_buckets = 20000;
 
   // Worst case scenario.
   if (K == N) { 
    std::sort(D.begin(), D.end());
    auto end = std::unique(D.begin(), D.end());
    return std::distance(D.begin(), end);
   }
 
   uint64_t* cache = new uint64_t[num_buckets];
   memset(cache, 0, sizeof(uint64_t) * num_buckets);
 
   int counter = 0;
   int previous_dish = 0;
   for (int i = 0; i < N; ++i) { 
    int current_value = D.at(i);
    // Ignore same previous dish like 3, followed by 3 in the following test
    // case only if the K is greater than 1. For 1 we are only interested in
    // immediate previous dish.
    if (K > 1 && previous_dish == current_value) continue;
    if ((i >= K + 1)) {
      int evict_item = D.at(i - K - 1);
      int cache_key = evict_item % num_buckets;
      uint64_t item_bitmask =
        (1 << static_cast<uint64_t>(std::ceil(evict_item / num_buckets)));
      cache[cache_key] &= ~item_bitmask;
    }
     
    int cache_key = current_value % num_buckets;
    uint64_t bitmask =
        (1 << static_cast<uint64_t>(std::ceil(current_value / num_buckets)));
    if ((cache[cache_key] & bitmask) != 0) {
      // Already consumed and within K distance.
      continue;
    }
     
    previous_dish = current_value;
    cache[cache_key] |= bitmask;
    counter++;
  }
   
  delete[] cache;
  return counter;
 }
 
int main(int argc, char** argv) {
  assert(getMaximumEatenDishCount(6, {1, 2, 3, 3, 2, 1}, 1) == 5);
  assert(getMaximumEatenDishCount(6, {1, 2, 3, 3, 2, 1}, 2) == 4);
  assert(getMaximumEatenDishCount(7, {1, 2, 1, 2, 1, 2, 1}, 2) == 2);
  return 0;
}