// https://leetcode.com/problems/wildcard-matching/description/

#include <cassert>
#include <iostream>
#include <string_view>
#include <vector>

namespace leetcode {

class WildcardMatching {
 public:
  explicit WildcardMatching(std::string_view pattern) : pattern_(pattern) {}

  void Compile();
  bool Match(std::string_view str);

 private:
  std::string_view pattern_;
  // Each state encodes following information.
  // Bit 0-7 The character to match.
  // Bit 8-16 The wildcard pattern information if any. zero if absent.
  std::vector<uint16_t> states_;
};

void WildcardMatching::Compile() {
  while (!pattern_.empty()) {
    char c = pattern_.front();
    pattern_.remove_prefix(1);
    uint16_t code = 0u;
    uint8_t* ptr = (uint8_t*)(&code);
    switch (c) {
      // Match any character.
      case '?': {
        ptr++;
        *ptr = 1;
        states_.push_back(code);
        break;
      }
      // Match any sequence of same characters.
      case '*': {
        ptr++;
        *ptr = 2;
        states_.push_back(code);
        break;
      }
      // Match exact character.
      default: {
        *ptr = c;
        states_.push_back(code);
        break;
      }
    }
  }
}

bool WildcardMatching::Match(std::string_view str) {
  for (auto state : states_) {
    uint8_t c = state & 0b0000000011111111;
    uint8_t s = (state & 0b1111111100000000) >> 8;

    switch (s) {
      // Match exact character.
      case 0: {
        if (str.front() != c) {
          return false;
        }
        str.remove_prefix(1);
        break;
      }
      // Allow any single character for '?' pattern.
      case 1: {
        str.remove_prefix(1);
        break;
      }
      // Allow any sequence of same characters. So skip all similar characters.
      case 2: {
        uint8_t c = str.front();
        str.remove_prefix(1);
        while (!str.empty() && str.front() == c) {
          str.remove_prefix(1);
        }
        break;
      }
    }
  }

  // If string is not empty, there are more characters to match.
  return str.empty();
}

}  // namespace leetcode

using leetcode::WildcardMatching;

int main(int argc, char** argv) {
  WildcardMatching wm1("ab?d*");
  wm1.Compile();
  assert(wm1.Match("abcdeeeeee"));

  WildcardMatching wm2("a");
  wm2.Compile();
  assert(!wm2.Match("aa"));
  assert(wm2.Match("a"));

  WildcardMatching wm3("*");
  wm3.Compile();
  assert(wm3.Match("aa"));
  assert(!wm3.Match("aaaab"));

  WildcardMatching wm4("?a");
  wm4.Compile();
  assert(wm4.Match("ca"));
  assert(!wm4.Match("cb"));
}
