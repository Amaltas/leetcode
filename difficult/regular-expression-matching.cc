// https://leetcode.com/problems/regular-expression-matching/description/
//
// $ clang++ -std=c++2b regular-expression-matching.cc -o regular-expression-matching
// $ ./regular-expression-matching

#include <cassert>
#include <iostream>
#include <string_view>
#include <vector>

namespace leetcode {

class Re {
 public:
  explicit Re(std::string_view pattern) : pattern_(pattern) {}
  void Compile();
  bool Match(std::string_view str);

 private:
  std::string_view pattern_;
  // Each state encodes following information:
  // Bit 0-7    The character to match.
  // Bit 8-15   The state.
  std::vector<uint16_t> states_;
};

void Re::Compile() {
  while (!pattern_.empty()) {
    char c = pattern_.front();
    pattern_.remove_prefix(1);
    uint16_t code = 0u;
    uint8_t* ptr = (uint8_t*)(&code);
    switch (c) {
      case '.':
        ptr++;
        *ptr = 1;
        states_.push_back(code);
        break;
      case '*': {
        ptr = (uint8_t*)(&states_.back());
        ptr++;
        *ptr = 2;
        break;
      }
      default: {
        *ptr = c;
        states_.push_back(code);
        break;
      }
    }
  }

  for (auto s : states_) {
    std::cout << s << std::endl;
  }
}

bool Re::Match(std::string_view str) {
  for (auto state : states_) {
    uint8_t c = state & 0b0000000011111111;
    uint8_t s = (state & 0b1111111100000000) >> 8;
    std::cout << c << " , " << s << std::endl;

    switch (s) {
      case 0: {
        std::cout << str.front() << " == " << c << std::endl;
        if (str.front() != c) {
          return false;
        }

        str.remove_prefix(1);
        break;
      }
      case 1: {
        str.remove_prefix(1);
        break;
      }
      case 2: {
        while (str.front() == c) {
          str.remove_prefix(1);
        }
      }
    }
  }

  return str.empty();
}

void TestPattern(std::string_view pattern, std::string_view str, bool matches) {
  leetcode::Re re(pattern);
  re.Compile();
  std::cout << "Matching: " << str << " against pattern: " << pattern << std::endl;
  assert((matches ? re.Match(str) : !re.Match(str)));
}

}  // namespace leetcode


int main(int argc, char** argv) {
  leetcode::TestPattern("amal.as*bohra", "amaltasbohra", true);
  leetcode::TestPattern("amal.as*bohra", "amaltabohra", true);
  leetcode::TestPattern("amal.as*bohra", "amaltasssssssbohra", true);
  leetcode::TestPattern("amat.as*bohra", "amaltasbohra", false);
  // -----------------------^
  leetcode::TestPattern("amaltas*", "amaltassssssssssssssssssssssssss", true);
  leetcode::TestPattern("3.14", "3.14", true);
}
