// https://leetcode.com/problems/valid-parentheses/description

// $ clang++-15 -std=c++2b valid-parentheses.cc -o valid-parentheses
// $ ./valid-parentheses

#include <cassert>
#include <stack>
#include <string_view>

namespace leetcode {

bool ValidParenthesis(std::string_view target) {
	std::stack<uint8_t> states;
	while (!target.empty()) {
		auto cur = target.front();
		switch (cur) {
			case '(':
				states.push(')');
				break;
			case '{':
				states.push('}');
				break;
			case '[':
				states.push(']');
				break;
			case ')':
			case '}':
			case ']': {
				if (cur != states.top()) {
					return false;
				}
				states.pop();
				break;
			}
			default:
				  return false;
		}
		target.remove_prefix(1);
	}

	return states.empty();
}

}  // namespace leetcode


int main(int argc, char** argv) {
	assert(leetcode::ValidParenthesis("([{}])") && "([{}]) is valid");
	assert(leetcode::ValidParenthesis("(((([{}]))))") && "(((([{}])))) is valid");
	assert(!leetcode::ValidParenthesis("((({])))") && "((({]))) is not valid");
}
