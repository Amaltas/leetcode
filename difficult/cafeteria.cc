// Puzzle https://gitlab.com/Amaltas/leetcode/-/raw/main/files/Screenshot_2023-10-14_at_1.24.19_AM.png

// Compile and run:
// $ clang++ -std=c++20 -o cafeteria cafeteria.cc
// $ ./cafeteria

#include <algorithm>
#include <cassert>
#include <vector>

using namespace std;
// Write any include statements here

uint64_t MaxSeatsBetween(uint64_t empty_seats, uint64_t distance, bool tail) {
  // If empty seats are less than distance return early.
  if (empty_seats < (tail ? distance + 1 : distance * 2 + 1)) {
    return 0;
  }
  
  return (empty_seats - (tail ? 0 : distance)) / (distance + 1);
}

uint64_t GetMaxAdditionalDinersCount(uint64_t N, uint64_t K, int M, vector<uint64_t> S) {
  // Write your code here
  uint64_t counter = 0;
  std::sort(S.begin(), S.end());
  
  // Count how many diners can sit before the first occupied seat.
  if (S.front() > K) {
    counter += MaxSeatsBetween(S.front() - 1, K, true);
  }
  
  // Count how many diners can sit between first and last occupied sit.
  for (int i = 1; i < S.size(); ++i) {
    counter += MaxSeatsBetween(S.at(i) - S.at(i - 1) - 1, K, false);
  }


  // Count how many diners can sit after the last occupied seat.
  if (S.back() < N) {
    counter += MaxSeatsBetween(N - S.back(), K, true);
  }
  
  return counter;
}


int main(int argc, char** argv) {
  assert(GetMaxAdditionalDinersCount(10, 1, 2, {2, 6}) == 3);
  assert(GetMaxAdditionalDinersCount(15, 2, 3, {11, 6, 14}) == 1);
}